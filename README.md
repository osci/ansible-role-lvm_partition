Ansible module to setup a lvm partition

# Usage

After creating the vollume group, it can be used like this:

```
---
- hosts: vm.example.org
  roles:
  - role: lvm_partition
    vg: data
    size: 3G
    lv_name: www_data
    path: /srv/www
    fstype: ext4
```

By default, the filesystem will be xfs, unless specified with the `fstype` option.

The `opts` and `passno` mount options can be specified too.

